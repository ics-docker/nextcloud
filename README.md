nextcloud
=========

[Nextcloud](https://github.com/nextcloud/docker) docker image with cron support.

Based on https://github.com/nextcloud/docker/tree/master/.examples/dockerfiles/cron/apache

Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/nextcloud:latest
```
