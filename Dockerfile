FROM nextcloud:27.1.11

LABEL maintainer "te-hung.tseng@ess.eu"

RUN apt-get update && apt-get install -y \
    supervisor \
    sudo \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && mkdir -p /var/log/supervisord /var/run/supervisord

COPY supervisord.conf /etc/supervisor/supervisord.conf

ENV NEXTCLOUD_UPDATE=1

CMD ["/usr/bin/supervisord"]
